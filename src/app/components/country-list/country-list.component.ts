import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Country } from '../../models/country.model';
import { ApiService } from '../../_shared/services/apiServices/api.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.css']
})
export class CountryListComponent implements OnInit {
   
   public countries : Country[] = [];
   public showSpinner:boolean = true;
   public showSearchField:boolean = false;
   public searchForm;

  constructor(private _apiService:ApiService , private _formBuilder: FormBuilder , private _snackBar:MatSnackBar) {

     this.searchForm = this._formBuilder.group({
         search: '',
    });
   }

  ngOnInit(): void {

    this._apiService.getCountries().subscribe({next:(responseData)=>{
          responseData.forEach((country:any)=> {
            this.countries.push({"name":country.name.common,"capital":country.capital,"subRegion":country.subregion,population:country.population,"flag":country.flags.png});
        });
        },error:(err)=>{
          this.showSpinner = false;
          this.openSnackBar(err.message,"Failure",10000)
         
        },complete:()=>{
            this.showSpinner = false;
            this.showSearchField = true;
            this.openSnackBar("Countries Loaded","Success",2000)
            
        }})
  }

    // Function to display response messages
    openSnackBar(message:string, type:string ,duration:number){
        this._snackBar.open(message,type,{
          duration:duration
      })
    }
}

   



