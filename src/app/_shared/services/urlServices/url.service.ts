import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

// This class is to define all the API urls for the app//
export class UrlService {

  // URL to get list of countries //
  COUNTRY_LIST = "https://restcountries.com/v3.1/all";

  constructor() { }


}
