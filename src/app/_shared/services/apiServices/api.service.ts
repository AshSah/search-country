import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { UrlService } from '../urlServices/url.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient:HttpClient, private url:UrlService) { }

  // GET API to get list of countries//
    getCountries():Observable<any>{
        return this.httpClient.get(this.url.COUNTRY_LIST,{headers: new HttpHeaders(this.getHeaders())})
    }


    // Get Header object for HTTP requests //
    private getHeaders() {
      let headers = { "Content-Type": "application/json" };
      return headers;
  }
}
