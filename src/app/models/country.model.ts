export class Country{

   public name:string;
   public capital:string;
   public subRegion:string;
   public population:number;
   public flag:string;


   constructor(name:string, capital:string, subRegion:string, population:number , flag:string){
       this.name = name;
       this.capital = capital;
       this.subRegion = subRegion;
       this.population = population;
       this.flag = flag;
   }

}