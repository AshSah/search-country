import { Pipe, PipeTransform } from '@angular/core';
import { Country } from '../models/country.model';

@Pipe({
  name: 'searchCountry'
})
export class SearchCountryPipe implements PipeTransform {

  transform(countries: Country[], searchInputText: string): any {
    
    // Case Insensitive Text input search 
    let lowerCaseInputText = searchInputText.toLowerCase();
    return (searchInputText) ? 
    countries.filter(country=> country.name.toLowerCase().includes(lowerCaseInputText)):countries;
    
  }

}
